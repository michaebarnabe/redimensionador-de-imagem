import os
from PIL import Image, ImageFile
import re

# Para evitar erros com arquivos de imagem truncados
ImageFile.LOAD_TRUNCATED_IMAGES = True

def resize_image(img, max_size, is_vertical):
    width, height = img.size
    if is_vertical:
        if height > max_size:
            ratio = max_size / float(height)
            new_size = (int(width * ratio), max_size)
            img = img.resize(new_size, Image.Resampling.LANCZOS)
    else:
        if width > max_size:
            ratio = max_size / float(width)
            new_size = (max_size, int(height * ratio))
            img = img.resize(new_size, Image.Resampling.LANCZOS)
    return img

def extract_number(filename):
    match = re.search(r'\d+', filename)
    return int(match.group()) if match else 0

def process_images(input_dir, output_dir):
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    for root, subdirs, _ in os.walk(input_dir):
        for subdir in subdirs:
            subdir_path = os.path.join(root, subdir)
            output_subdir_path = os.path.join(output_dir, os.path.relpath(subdir_path, input_dir))
            if not os.path.exists(output_subdir_path):
                os.makedirs(output_subdir_path)
            
            for file in os.listdir(subdir_path):
                if file.lower().endswith(('.jpg', '.jpeg', '.png', '.bmp', '.gif', '.tiff')):
                    img_path = os.path.join(subdir_path, file)
                    try:
                        with Image.open(img_path) as img:
                            width, height = img.size
                            is_vertical = height > width
                            max_size = 1200
                            img = resize_image(img, max_size, is_vertical)
                            img = img.convert('RGB')  # Convert to JPG

                            file_number = extract_number(file)

                            output_path = os.path.join(output_subdir_path, f"{subdir}-{file_number}.jpg")
                            img.save(output_path, quality=100, dpi=(72, 72))  # Save with web quality and 72 DPI
                            print(f"Processed and saved {output_path}")
                    except Exception as e:
                        print(f"Error processing file {file}: {e}")

process_images('input', 'output')
