# Projeto de Redimensionamento de Imagens (Windows)

Este projeto redimensiona imagens em várias subpastas dentro de uma pasta de entrada, mantendo a estrutura das subpastas na pasta de saída. As imagens são redimensionadas para uma altura máxima de 1200px (se verticais) ou largura máxima de 1200px (se horizontais). Além disso, as imagens são convertidas para o formato JPG com qualidade adequada para a web.

## Pré-requisitos

- Python 3.6 ou superior

## Instruções de Instalação

1. **Baixar e Instalar Python:**

   - Acesse o site oficial do Python: [Python Downloads](https://www.python.org/downloads/)
   - Baixe o instalador do Python para Windows.
   - Execute o instalador e certifique-se de marcar a opção "Add Python to PATH" antes de instalar.

2. **Instalar Dependências:**

   Abra o Prompt de Comando e execute:
   ```sh
   pip install pillow



## **Clonando Repositorio:**
Para clonar o repositório, abra o Prompt de Comando e execute:
git clone: via SSH ou HTTPS ou Baixo o Arquivo ZIP.



## **Forma de Uso**
1. **Estrutura de Pastas**
A Estrutura ja esta pronta para uso:
   1. Adicione suas Sub Pastas dentro da "INPUT" [Exemplo: lote1, lote2, etc...]
   2. Após executar o SCRIPT o resulto estará na pasta OUTPUT


## **Executar o Script:**
Navegue até a pasta do projeto no Prompt de Comando e execute o script:

-- python process_images.py

O script processará todas as imagens na pasta input, redimensionará conforme necessário e salvará as imagens na pasta output, mantendo a estrutura das subpastas e renomeando os arquivos conforme o nome da subpasta e o número extraído do nome do arquivo (ou 0 se não houver número).

## **Problemas Conhecidos**

Certifique-se de que as imagens na pasta input estejam em um dos seguintes formatos: .jpg, .jpeg, .png, .bmp, .gif, .tiff.
Se encontrar algum erro de arquivo truncado, o script está configurado para ignorar esses erros e continuar processando as imagens.

## **Contribuições**

Contribuições são bem-vindas! Sinta-se à vontade para abrir issues e pull requests.